package com.example.stick_control

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import java.lang.Integer.parseInt
import java.util.*


class MainActivity : AppCompatActivity() {
    private var mTimer: Timer? = null
    private lateinit var mMyTimerTask: MyTimerTask
    private lateinit var myHandler: MyHandler
    private lateinit var note_view: NoteView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        note_view = NoteView(
            findViewById(R.id.notesView),
            resources,
            packageName
        )
        myHandler = MyHandler(note_view, findViewById(R.id.textViewTimerValue))

    }

    override fun onResume() {
        super.onResume()
        note_view.showNotes()
    }

    fun openSettings(item: MenuItem){
        if (item.getItemId() === R.id.go_to_settings) {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }
    }

    fun onStartButtonClick(view: View?) {
        val playButton : ImageButton = findViewById(R.id.imageButtonPlay);
        playButton.visibility = View.INVISIBLE
        val pauseButton : ImageButton = findViewById(R.id.imageButtonPause);
        pauseButton.visibility = View.VISIBLE

        if (this.mTimer != null) {
            this.mTimer!!.cancel()
            this.mTimer!!.purge()
        }
        this.mTimer = Timer()
        this.mMyTimerTask = MyTimerTask()

        mMyTimerTask.myHandler = this.myHandler

        this.mTimer!!.schedule(this.mMyTimerTask, 1000, 1000)

    }

    fun onPauseButtonClick(view: View?) {
        val playButton : ImageButton = findViewById(R.id.imageButtonPlay);
        playButton.visibility = View.VISIBLE
        val pauseButton : ImageButton = findViewById(R.id.imageButtonPause);
        pauseButton.visibility = View.INVISIBLE

        this.mTimer!!.cancel()
    }

    fun onResetButtonClick(view: View?) {
        this.myHandler.sendEmptyMessage(1);
    }

    internal class MyTimerTask : TimerTask() {
        public var myHandler :Handler? = null
        override fun run() {
            myHandler?.sendEmptyMessage(0);
        }
    }

    class MyHandler(val note_view: NoteView, private val out: TextView): Handler() {
        var sec_numb = 0
        var min_numb = 0

        @SuppressLint("SetTextI18n")
        override fun handleMessage(msg: Message) {
            if (msg.what == 1) {
                this.sec_numb = 0
                this.min_numb = 0
                this.note_view.resetNotes()
                this.note_view.showNotes()
            }
            else
                this.sec_numb += 1

            //обновляем значение в таймере
            val SEC_IN_MINUTE = 60
            val min_numb_new = sec_numb /SEC_IN_MINUTE
            this.out.text = "$min_numb_new:${sec_numb % SEC_IN_MINUTE}"

            if (min_numb_new > this.min_numb)
            {
                min_numb = min_numb_new
                note_view.nextNotes()
            }

        }
    }
}

class NoteView(
    private val out_image_view: ImageView,
    private val res: Resources, private val pack_name: String
) {
    private var row_numb = 1

    init {
        showNotes()
    }
    fun resetNotes(){
        row_numb = 1
    }
    fun nextNotes(): Boolean{
        row_numb++
        val maxRowOnPage = 24
        if (row_numb <= maxRowOnPage) {
            showNotes();
            return false
        }
        else
        {
            row_numb = 1
            out_image_view.setImageResource(
                res.getIdentifier("nice", "drawable", pack_name)
            )
            val maxTrainingNumber = 7
            var trainingNumber = getCurrentTrainingNumb()
            if (trainingNumber < maxTrainingNumber){
                setCurrentTrainingNumb(trainingNumber + 1)
            }
            else
            {
                setCurrentTrainingNumb(1)
                val page = getCurrentPageNumb()
                setCurrentPageNumb(page + 1)
            }
            return true
        }
    }
    fun showNotes(){
        val pageNumb = getCurrentPageNumb()
        val maxPageNum = 3
        val minPageNum = 1
        if (maxPageNum > 3 || minPageNum < 1) {
            return
        }

        val imageName : String = "page_${pageNumb}_${this.row_numb}"
        out_image_view.setImageResource(
            res.getIdentifier(imageName, "drawable", pack_name)
        )
    }

    private fun getCurrentPageNumb(): Int {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(out_image_view.context)
        val name = sharedPreferences.getString("page_number", "")
        return parseInt(name)
    }

    private fun setCurrentPageNumb(page : Int){
        val pageString = page.toString()
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(out_image_view.context)
        with (sharedPreferences.edit()) {
            putString("page_number", pageString)
            apply()
        }
    }

    private fun getCurrentTrainingNumb(): Int {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(out_image_view.context)
        val name = sharedPreferences.getString("training_number", "")
        return parseInt(name)
    }

    private fun setCurrentTrainingNumb(training : Int){
        val trainingString = training.toString()
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(out_image_view.context)
        with (sharedPreferences.edit()) {
            putString("training_number", trainingString)
            apply()
        }
    }
}
